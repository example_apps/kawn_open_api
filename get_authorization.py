"""
# Set your env with CLIENT_ID and CLIENT SECRET that you have copied it from
# Kawn Dashboard.
"""

import base64, environ, requests, os

env = environ.Env()
ROOT_DIR = environ.Path(__file__) - 1
env.read_env(str(ROOT_DIR.path('.env')))

client_id = env.str("CLIENT_ID", "DEFAULT_CLIENT_ID_WONT_WORK")
client_secret = env.str("CLIENT_SECRET", "DEFAULT_CLIENT_SECRET_WONT_WORK")

credential = "{}:{}".format(client_id, client_secret)
authorization = base64.b64encode(credential.encode('utf-8'))

authorization = authorization.decode('unicode_escape')

headers = {
    'Authorization': f"Basic {authorization}",
    'Cache-Control': 'no-cache',
}
data = {
    'grant_type': 'client_credentials',
}

response = requests.post(url="{}/oauth2/token/".format(env.str("HOST_URL")), headers=headers, data=data)
response = response.json()


print(response['access_token'])
# Rewrite your ACCESS_TOKEN from environment variable with the access_token from response