import base64, environ, requests, os

env = environ.Env()
ROOT_DIR = environ.Path(__file__) - 1
env.read_env(str(ROOT_DIR.path('.env')))

access_token = env.str("ACCESS_TOKEN", "DEFAULT_ACCESS_TOKEN_WONT_WORK")

headers = {
    'Authorization': f"Bearer {access_token}",
}

response = requests.get(url="{}/developer/v1/products/".format(env.str("HOST_URL")), headers=headers)

"""
# if return is true the endpoint is successfully accessed, now you can customizing like using ordering
# filtering and anything else that we provided for your systems
# ^^R
"""
print(response.status_code == 200)